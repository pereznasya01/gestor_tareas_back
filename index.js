const express = require('express');
const app = express();
const http = require("http");
const cors = require("cors");
const { Server } = require("socket.io");
const mysql = require('mysql2');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const cookieParser = require('cookie-parser');

// Configuración del servidor y de Socket.IO
const server = http.createServer(app);
const socketIO = new Server(server, {
    cors: {
      origin: "http://localhost:5173",
    },
  });

  app.use(cors());
  app.use(express.json());

  const connection = mysql.createConnection({
    user: 'root',   
    password: 'Mew2604', 
    database: 'gestor'     
  });

  connection.connect((err) => {
    if (err) {
      console.error('Error de conexión a la base de datos: ' + err.stack);
      return;
    }
    console.log('Conectado a la base de datos como id ' + connection.threadId);
  });

  socketIO.on("connection", (socket) => {

    // Endpoint para verificar nombre de usuario y correo electrónico antes del registro
socket.on("checkUser", async (data) => {
  const { nombre, correo } = data;

  // Verificar si el nombre de usuario o correo electrónico ya existen en la base de datos
  const checkUserQuery = 'SELECT * FROM usuarios WHERE nombre = ? OR correo = ?';
  connection.query(checkUserQuery, [nombre, correo], async (err, results) => {
    if (err) {
      console.error('Error al verificar el nombre de usuario y correo: ' + err.stack);
      socket.emit('checkUserResponse', { error: { message: 'Error al verificar el nombre de usuario y correo' } });
      return;
    }

    if (results.length > 0) {
      // Si ya existe un usuario con el mismo nombre o correo
      let errorMessage = '';
      results.forEach((user) => {
        if (user.nombre === nombre) {
          errorMessage += `El nombre de usuario '${nombre}' ya está registrado. `;
        }
        if (user.correo === correo) {
          errorMessage += `El correo electrónico '${correo}' ya está registrado. `;
        }
      });
      socket.emit('checkUserResponse', { error: { message: errorMessage.trim() } });
    } else {
      // Si no hay duplicados, enviar respuesta sin error
      socket.emit('checkUserResponse', { });
    }
  });
});

// Endpoint para registrar un nuevo usuario
socket.on("register", async (data) => {
  const { nombre, contraseña, correo, telefono } = data;

  // Verificar campos vacíos
  if (!nombre || !contraseña || !correo || !telefono) {
    socket.emit('registerError', { message: 'Nombre, contraseña, correo y teléfono son requeridos' });
    return;
  }

  // Hash de la contraseña
  const hashedPassword = await bcrypt.hash(contraseña, 10);

  // Insertar usuario en la base de datos
  const query = 'INSERT INTO usuarios (nombre, contraseña, correo, telefono) VALUES (?, ?, ?, ?)';
  connection.query(query, [nombre, hashedPassword, correo, telefono], (err, results) => {
    if (err) {
      console.error('Error al registrar el usuario: ' + err.stack);
      socket.emit('registerError', { message: 'Error al registrar el usuario' });
      return;
    }
    console.log('Usuario registrado con éxito');
    socket.emit('registerSuccess', { message: 'Usuario registrado con éxito' });
  });
});


    socket.on("login", (data) => {
      const { nombre, contraseña } = data;
    
      // Validar entrada
      if (!nombre || !contraseña) {
        socket.emit('loginError', { message: 'Nombre y contraseña son requeridos' });
        return;
      }
    
      const query = 'SELECT * FROM usuarios WHERE nombre = ?';
      connection.query(query, [nombre], async (err, results) => {
        if (err) {
          console.error('Error al buscar el usuario: ' + err.stack);
          socket.emit('loginError', { message: 'Error al buscar el usuario' });
          return;
        }
    
        if (results.length === 0) {
          socket.emit('loginError', { message: 'Usuario no encontrado' });
          return;
        }
    
        const user = results[0];
        const match = await bcrypt.compare(contraseña, user.contraseña);
    
        if (!match) {
          socket.emit('loginError', { message: 'Contraseña incorrecta' });
          return;
        }

        const token = jwt.sign({ userId: user.id }, 'Mew04', { expiresIn: '24h' });
        socket.emit('loginSuccess', { token });
      });
    });

    socket.on("addTask", async (data) => {
      const { nombre, descripcion, fecha_vencimiento, prioridad } = data;
      const token = data.token; // Suponemos que el token es parte de los datos enviados

      if (!token) {
        socket.emit('addTaskError', { message: 'Token no proporcionado' });
        return;
      }

      jwt.verify(token, 'Mew04', (err, decoded) => {
        if (err) {
          console.error('Error al verificar el token:', err.message);
          socket.emit('addTaskError', { message: 'Token inválido' });
          return;
        }

        const userId = decoded.userId;

        if (!nombre) {
          socket.emit('addTaskError', { message: 'Nombre es requerido' });
          return;
        }
      
        // Validar prioridad con opciones del ENUM
        const prioridadesValidas = ['Alta', 'Media', 'Baja'];
        if (prioridad && !prioridadesValidas.includes(prioridad)) {
          socket.emit('addTaskError', { message: 'Prioridad no válida' });
          return;
        }
      
        const query = 'INSERT INTO tareas (nombre, descripcion, estado, fecha_vencimiento, prioridad, usuario_id) VALUES (?, ?, ?, ?, ?, ?)';
        connection.query(query, [nombre, descripcion, 'Pendiente', fecha_vencimiento, prioridad, userId], (err, results) => {
          if (err) {
            console.error('Error al crear la tarea: ' + err.stack);
            socket.emit('addTaskError', { message: 'Error al crear la tarea' });
            return;
          }
          console.log('Tarea creada con éxito');
          socket.emit('addTaskSuccess', { message: 'Tarea creada con éxito' });
        });
      });
    });

    socket.on("getTasks", (data) => {
      const token = data.token;
    
      if (!token) {
        socket.emit('getTasksError', { message: 'Token no proporcionado' });
        return;
      }
    
      jwt.verify(token, 'Mew04', (err, decoded) => {
        if (err) {
          console.error('Error al verificar el token:', err.message);
          socket.emit('getTasksError', { message: 'Token inválido' });
          return;
        }
    
        const userId = decoded.userId;
    
        // Ordenar las tareas por prioridad
        const query = 'SELECT * FROM tareas WHERE usuario_id = ? ORDER BY FIELD(prioridad, "Alta", "Media", "Baja")';
        connection.query(query, [userId], (err, results) => {
          if (err) {
            console.error('Error al obtener las tareas: ' + err.stack);
            socket.emit('getTasksError', { message: 'Error al obtener las tareas' });
            return;
          }
          socket.emit('getTasksSuccess', results);
        });
      });
    });
    

    socket.on("addNoteToTask", (data) => {
      const { taskId, nota, token } = data;

      if (!token) {
          socket.emit('addNoteError', { message: 'Token no proporcionado' });
          return;
      }

      jwt.verify(token, 'Mew04', (err, decoded) => {
          if (err) {
              console.error('Error al verificar el token:', err.message);
              socket.emit('addNoteError', { message: 'Token inválido' });
              return;
          }

          const userId = decoded.userId;

          const query = 'UPDATE tareas SET notas = ? WHERE id = ? AND usuario_id = ?';
          connection.query(query, [nota, taskId, userId], (err, results) => {
              if (err) {
                  console.error('Error al agregar nota a la tarea: ' + err.stack);
                  socket.emit('addNoteError', { message: 'Error al agregar nota a la tarea' });
                  return;
              }
              console.log('Nota agregada a la tarea con éxito');
              socket.emit('addNoteSuccess', { message: 'Nota agregada a la tarea con éxito' });
          });
      });
  });

  socket.on("updateTask", (data) => {
    const { taskId, updatedTask, token } = data;
  
    jwt.verify(token, 'Mew04', (err, decoded) => {
      if (err) {
        console.error('Error al verificar el token:', err.message);
        socket.emit('updateTaskError', { message: 'Token inválido' });
        return;
      }
  
      const userId = decoded.userId;
  
      // Verificar si el usuario tiene permisos sobre la tarea
      const checkTaskQuery = 'SELECT * FROM tareas WHERE id = ? AND usuario_id = ?';
      connection.query(checkTaskQuery, [taskId, userId], (err, results) => {
        if (err) {
          console.error('Error al verificar la tarea:', err.message);
          socket.emit('updateTaskError', { message: 'Error al verificar la tarea' });
          return;
        }
  
        if (results.length === 0) {
          socket.emit('updateTaskError', { message: 'Tarea no encontrada o no tienes permisos' });
          return;
        }
  
        const updateTaskQuery = 'UPDATE tareas SET nombre = ?, descripcion = ?, estado = ?, fecha_vencimiento = ?, prioridad = ?, notas = ? WHERE id = ?';
        const { nombre, descripcion, estado, fecha_vencimiento, prioridad, notas } = updatedTask;
  
        // Asegurarse de que la fecha esté en el formato correcto
        const formattedFechaVencimiento = fecha_vencimiento.split('T')[0];
  
        connection.query(updateTaskQuery, [nombre, descripcion, estado, formattedFechaVencimiento, prioridad, notas, taskId], (err, results) => {
          if (err) {
            console.error('Error al actualizar la tarea:', err.message);
            socket.emit('updateTaskError', { message: 'Error al actualizar la tarea' });
            return;
          }
  
          // Obtener la tarea actualizada desde la base de datos y emitir el éxito
          const getUpdatedTaskQuery = 'SELECT * FROM tareas WHERE id = ?';
          connection.query(getUpdatedTaskQuery, [taskId], (err, results) => {
            if (err) {
              console.error('Error al obtener la tarea actualizada:', err.message);
              socket.emit('updateTaskError', { message: 'Error al obtener la tarea actualizada' });
              return;
            }
  
            const updatedTask = results[0];
            socket.emit('updateTaskSuccess', updatedTask);
          });
        });
      });
    });
  });
  

  socket.on("deleteTask", (data) => {
    const { taskId, token } = data;
  
    if (!token) {
      socket.emit('deleteTaskError', { message: 'Token no proporcionado' });
      return;
    }
  
    jwt.verify(token, 'Mew04', (err, decoded) => {
      if (err) {
        console.error('Error al verificar el token:', err.message);
        socket.emit('deleteTaskError', { message: 'Token inválido' });
        return;
      }
  
      const userId = decoded.userId;
  
      // Verificar si el usuario tiene permisos sobre la tarea
      const checkTaskQuery = 'SELECT * FROM tareas WHERE id = ? AND usuario_id = ?';
      connection.query(checkTaskQuery, [taskId, userId], (err, results) => {
        if (err) {
          console.error('Error al verificar la tarea:', err.message);
          socket.emit('deleteTaskError', { message: 'Error al verificar la tarea' });
          return;
        }
  
        if (results.length === 0) {
          socket.emit('deleteTaskError', { message: 'Tarea no encontrada o no tienes permisos' });
          return;
        }
  
        // Eliminar la tarea de la base de datos
        const deleteTaskQuery = 'DELETE FROM tareas WHERE id = ?';
        connection.query(deleteTaskQuery, [taskId], (err, results) => {
          if (err) {
            console.error('Error al eliminar la tarea:', err.message);
            socket.emit('deleteTaskError', { message: 'Error al eliminar la tarea' });
            return;
          }
  
          console.log('Tarea eliminada con éxito');
          socket.emit('deleteTaskSuccess', { message: 'Tarea eliminada con éxito' });
        });
      });
    });
  });
  
  socket.on("getUserById", (data) => {
    const { token } = data;
    if (!token) {
      socket.emit('getUserError', { message: 'Token no proporcionado' });
      return;
    }
    
    jwt.verify(token, 'Mew04', (err, decoded) => {
      if (err) {
        console.error('Error al verificar el token:', err.message);
        socket.emit('getUserError', { message: 'Token inválido' });
        return;
      }
  
      const userId = decoded.userId
  
      const query = 'SELECT * FROM usuarios WHERE id = ?';
      connection.query(query, [userId], (err, results) => {
        if (err) {
          console.error('Error al obtener el usuario: ' + err.stack);
          socket.emit('getUserError', { message: 'Error al obtener el usuario' });
          return;
        }
  
        if (results.length === 0) {
          socket.emit('getUserError', { message: 'Usuario no encontrado' });
          return;
        }
  
        socket.emit('getUserSuccess', results[0]);
      });
    });
  });

  socket.on("updateUser", (data) => {
    const { updatedUser, token } = data;

    if (!token) {
      socket.emit('updateUserError', { message: 'Token no proporcionado' });
      return;
    }

    jwt.verify(token, 'Mew04', (err, decoded) => {
      if (err) {
        console.error('Error al verificar el token:', err.message);
        socket.emit('updateUserError', { message: 'Token inválido' });
        return;
      }

      const userId = decoded.userId; // Obtener userId del token
      const { nombre, contrasena, correo, telefono } = updatedUser;

      let updateUserQuery = `
        UPDATE usuarios 
        SET nombre = ?, ${contrasena ? 'contraseña = ?, ' : ''}correo = ?, telefono = ? 
        WHERE id = ?
      `;

      let queryParams;
      if (contrasena) {
        const hashedPassword = bcrypt.hashSync(contrasena, 10);
        queryParams = [nombre, hashedPassword, correo, telefono, userId];
      } else {
        updateUserQuery = updateUserQuery.replace('contraseña = ?, ', '');
        queryParams = [nombre, correo, telefono, userId];
      }

      connection.query(updateUserQuery, queryParams, (err, results) => {
        if (err) {
          console.error('Error al actualizar el usuario: ' + err.stack);
          socket.emit('updateUserError', { message: 'Error al actualizar el usuario' });
          return;
        }

        socket.emit('updateUserSuccess', { message: 'Usuario actualizado con éxito' });
      });
    });
  });
  
  socket.on("deleteUser", (data) => {
    const { token } = data;
  
    if (!token) {
      socket.emit('deleteUserError', { message: 'Token no proporcionado' });
      return;
    }
  
    jwt.verify(token, 'Mew04', (err, decoded) => {
      if (err) {
        console.error('Error al verificar el token:', err.message);
        socket.emit('deleteUserError', { message: 'Token inválido' });
        return;
      }
  
      const userId = decoded.userId; // Obtener userId del token
  
      const deleteTasksQuery = 'DELETE FROM tareas WHERE usuario_id = ?';
      connection.query(deleteTasksQuery, [userId], (err, taskResults) => {
        if (err) {
          console.error('Error al borrar las tareas del usuario:', err.stack);
          socket.emit('deleteUserError', { message: 'Error al borrar las tareas del usuario' });
          return;
        }
  
        const deleteUserQuery = 'DELETE FROM usuarios WHERE id = ?';
        connection.query(deleteUserQuery, [userId], (err, userResults) => {
          if (err) {
            console.error('Error al borrar el usuario:', err.stack);
            socket.emit('deleteUserError', { message: 'Error al borrar el usuario' });
            return;
          }
  
          socket.emit('deleteUserSuccess', { message: 'Usuario y sus tareas borrados con éxito' });
        });
      });
    });
  });  
  

});

  const PORT = process.env.PORT || 5000;
  server.listen(PORT, () => {
    console.log(`listening on ${PORT}`);
  });
